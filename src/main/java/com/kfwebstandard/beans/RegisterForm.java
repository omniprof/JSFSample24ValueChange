package com.kfwebstandard.beans;

import java.io.Serializable;
import java.util.Locale;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("form")
@RequestScoped
public class RegisterForm implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(RegisterForm.class);

    private String streetAddress;
    private String city;
    private String state;
    private String country;

    private static final Locale[] countries = {Locale.CANADA, Locale.US};

    public Locale[] getCountries() {
        return countries;
    }

    public void setStreetAddress(String newValue) {
        streetAddress = newValue;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setCity(String newValue) {
        city = newValue;
    }

    public String getCity() {
        return city;
    }

    public void setState(String newValue) {
        state = newValue;
    }

    public String getState() {
        return state;
    }

    public void setCountry(String newValue) {
        country = newValue;
    }

    public String getCountry() {
        return country;
    }

    /**
     * Change Listener
     *
     * @param event
     */
    public void countryChanged(ValueChangeEvent event) {
        for (Locale loc : countries) {
            if (loc.getCountry().equals(event.getNewValue())) {
                FacesContext.getCurrentInstance().getViewRoot().setLocale(loc);
            }
        }
    }
}
